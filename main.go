package main

import (
	"os"

	logger "github.com/hieuphanuit/golang-simple-logger"
)

func main() {
	// Khởi tạo file để lưu log
	f, _ := os.OpenFile("./test_log.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	config := &logger.LoggerConfig{
		Flag:    logger.FLAG_DEBUGP,
		Outputs: []*os.File{os.Stdout, f}, // For both file and terminal
	}
	logger.SetConfig(config)

	// Lưu log vào file
	logger.DebugP("DEBUG LOGGER")
	logger.Error("ERROR LOGGER")
	logger.Info("Info LOGGER")

	// time.Sleep(time.Hour * 1)
}
